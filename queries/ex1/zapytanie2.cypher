MATCH(hugo {name: 'Hugo Weaving'})-[:ACTED_IN]->(movie) <-[:DIRECTED]-(director)
RETURN director;