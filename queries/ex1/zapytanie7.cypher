CREATE(Avengers:Movie {title:   'Captain America: The First Avenger', released: 2011,
                       tagline: 'Steve Rogers, a rejected military soldier transforms into Captain America after taking a dose of a "Super-Soldier serum". But being Captain America comes at a price as he attempts to take down a war monger and a terrorist organization.'})
CREATE (JoeJohnston:Person {name: 'Joe Johnston', born: 1950})
CREATE (ChristopherMarkus:Person {name: 'Christopher Markus', born: 1973})
CREATE (ChrisEvans:Person {name: 'Chris Evans', born: 1981})
CREATE (HayleyAtwell:Person {name: 'Hayley Atwell', born: 1982})
CREATE (SamuelLJackson:Person {name: 'Samuel L. Jackson', born: 1948})

CREATE
  (JoeJohnston)-[:DIRECTED]->(Avengers),
  (ChristopherMarkus)-[:WROTE]->(Avengers),
  (ChrisEvans)-[:ACTED_IN {roles: ['Captain America']}]->(Avengers),
  (HayleyAtwell)-[:ACTED_IN {roles: ['Peggy Carter']}]->(Avengers),
  (SamuelLJackson)-[:ACTED_IN {roles: ['Nick Fury']}]->(Avengers);

MATCH (movie {title: 'Captain America: The First Avenger'})<-[:ACTED_IN|:DIRECTED|:WROTE]-(participant)
RETURN movie, participant;