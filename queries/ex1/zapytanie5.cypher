MATCH (director)-[:DIRECTED]->(movie), (director)-[:WROTE]->(movie)
RETURN director.name, movie.title;