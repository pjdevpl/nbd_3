MATCH (actor)-[acted:ACTED_IN]->(movie)
RETURN actor.name, count(acted)
  ORDER BY actor.name;