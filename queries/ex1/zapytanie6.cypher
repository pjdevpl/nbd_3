MATCH (hugo {name: 'Hugo Weaving'})-[:ACTED_IN]->(movie), (keanu {name: 'Keanu Reeves'})-[:ACTED_IN]->(movie)
RETURN movie;