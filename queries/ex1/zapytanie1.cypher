MATCH(hugo {name: 'Hugo Weaving'})-[:ACTED_IN]->(movie)
RETURN hugo, movie;