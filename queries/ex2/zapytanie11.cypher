MATCH p = (d {name: 'Darjeeling'})-[r*]->(s {name: 'Sandakphu'})
WITH d, collect(r) AS stages_combined, s
UNWIND stages_combined AS stages
WITH d, trim(reduce(c = '', cc IN tail(stages) | c + ' ' + startNode(cc).name)) AS stage_cities_concat, s,
     reduce(dist = 0, stage IN stages | dist + stage.distance) AS distance
RETURN DISTINCT d.name AS start_city, stage_cities_concat as via, s.name AS end_city, distance
  ORDER BY distance asc;