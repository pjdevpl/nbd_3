MATCH p = (d {name: 'Darjeeling'})-[:twowheeler * ]->(s)
  WHERE ALL(r IN relationships(p)
    WHERE r.summer = 'true')
RETURN s;