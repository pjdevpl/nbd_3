MATCH p = shortestPath((d {name: 'Darjeeling'})-[*]->(s {name: 'Sandakphu'}))
  WHERE ALL(r IN relationships(p)
    WHERE r.winter = 'true')
RETURN p;