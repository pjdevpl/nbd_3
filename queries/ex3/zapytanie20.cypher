MATCH (a)<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c),
      (fa)<-[:ASSIGN]-(t1),
      (fb)<-[:ASSIGN]-(t2)
  WHERE a.name <> b.name AND b.name <> c.name AND c.name <> a.name
CREATE (a)-[:CONNECTED {overall_price: t1.price + t2.price, overall_route: [a.name, b.name, c.name]}]->(c);

MATCH ()-[r:CONNECTED]->()
WITH min(r.overall_price) AS min_price
MATCH ()-[r:CONNECTED]->()
  WHERE r.overall_price = min_price
WITH r.overall_route AS nodes, min_price
MATCH (a)<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c),
      (fa)<-[:ASSIGN]-(t1),
      (fb)<-[:ASSIGN]-(t2)
  WHERE [a.name, b.name, c.name] = nodes AND t1.price + t2.price = min_price
WITH a, b, c, fa, fb
MATCH ()-[r:CONNECTED]->()
DELETE r
RETURN DISTINCT a, b, c, fa, fb;