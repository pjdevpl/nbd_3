MATCH
  (a)<-[:ORIGIN]-(fa) -[:DESTINATION]->(b)
with a, b, fa.airline as airline
CREATE (a)-[:CONNECTED {airline: airline}]->(b);

MATCH
  (a)<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c)
  WHERE fa.airline = fb.airline
with a, b, c, fa.airline as airline
CREATE (a)-[:CONNECTED {airline: airline}]->(b)
CREATE (a)-[:CONNECTED {airline: airline}]->(c);

MATCH
  (a)<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c) <-[:ORIGIN]-(fc)-[:DESTINATION]->(d)
WHERE fa.airline = fb.airline and fb.airline = fc.airline
with a, b, c, d, fa.airline as airline
CREATE (a)-[:CONNECTED {airline: airline}]->(b)
CREATE (a)-[:CONNECTED {airline: airline}]->(c)
CREATE (a)-[:CONNECTED {airline: airline}]->(d);

match (a) -[c:CONNECTED]-> ()
return count(distinct a) as cities_count, c.airline order by c.airline;

MATCH ()-[r:CONNECTED]->()
DELETE r;