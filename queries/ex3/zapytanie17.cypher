/* Find the cheapest direct flight */
MATCH (a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b {name: 'DAY'}),
      (fa)<-[:ASSIGN]-(t1)
WITH a, b, t1.price as price
  ORDER BY price
  LIMIT 1
CREATE (a)-[:CHEAPEST {overall_price: price}]->(b);

/* Find the cheapest 1-transfer flight */
MATCH (a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c {name: 'DAY'}),
      (fa)<-[:ASSIGN]-(t1),
      (fb)<-[:ASSIGN]-(t2)
WITH a, b, c, (t1.price + t2.price) as price
  ORDER BY price
  LIMIT 1
CREATE (a)-[:CHEAPEST {overall_price: price}]->(b)
CREATE (b)-[:CHEAPEST {overall_price: price}]->(c);

/* Find the cheapest 2-transfer flight */
MATCH(a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c) <-[:ORIGIN]-(fc)
       -[:DESTINATION]->(d {name: 'DAY'}),
     (fa)<-[:ASSIGN]-(t1),
     (fb)<-[:ASSIGN]-(t2),
     (fc)<-[:ASSIGN]-(t3)
WITH a, b, c, d, (t1.price + t2.price + t3.price) AS price
  ORDER BY price
  LIMIT 1
CREATE (a)-[:CHEAPEST {overall_price: price}]->(b)
CREATE (b)-[:CHEAPEST {overall_price: price}]->(c)
CREATE (c)-[:CHEAPEST {overall_price: price}]->(d);

/* Show the cheapest connection in overall */
MATCH (a)-[r:CHEAPEST]->(b)
WITH min(r.overall_price) as min_price
MATCH (a)-[r:CHEAPEST]->(b)
WHERE r.overall_price = min_price
return a, b, r;

/* Cleanup */
MATCH ()-[r:CHEAPEST]->()
DELETE r;