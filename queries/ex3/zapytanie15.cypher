MATCH (a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c) <-[:ORIGIN]-(fc)-[:DESTINATION]->(d),
      (fa)<-[:ASSIGN]-(t1),
      (fb)<-[:ASSIGN]-(t2),
      (fc)<-[:ASSIGN]-(t3)
  WHERE b.name <> 'LAX' and c.name <> 'LAX' and d.name <> 'LAX'
CREATE (a)-[:CONNECTED {price: t1.price}]-> (b)
CREATE (a)-[:CONNECTED {price: t1.price + t2.price}]-> (c)
CREATE (a)-[:CONNECTED {price: t1.price + t2.price + t3.price}]->(d);

MATCH (a {name: 'LAX'}) -[c:CONNECTED]-> (dest)
  WHERE c.price < 3000
return distinct dest;

MATCH ()-[r:CONNECTED]->()
DELETE r;