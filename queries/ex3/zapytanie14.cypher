MATCH (f:Flight)-[:ORIGIN]->(airport)
RETURN airport, count(f);