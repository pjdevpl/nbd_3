MATCH (a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b {name: 'DAY'}),
      (fa)<-[:ASSIGN]-(t1)
CREATE (a)-[:CONNECTED {price: t1.price, transfers: []}]-> (b);

MATCH (a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c {name: 'DAY'}),
      (fa)<-[:ASSIGN]-(t1),
      (fb)<-[:ASSIGN]-(t2)
CREATE (a)-[:CONNECTED {price: t1.price + t2.price, transfers: [b.name]}]-> (c);

MATCH (a {name: 'LAX'})<-[:ORIGIN]-(fa) -[:DESTINATION]->(b) <-[:ORIGIN]-(fb) -[:DESTINATION]->(c) <-[:ORIGIN]-(fc)-[:DESTINATION]->(d {name: 'DAY'}),
      (fa)<-[:ASSIGN]-(t1),
      (fb)<-[:ASSIGN]-(t2),
      (fc)<-[:ASSIGN]-(t3)
CREATE (a)-[:CONNECTED {price: t1.price + t2.price + t3.price, transfers: [b.name, c.name]}]->(d);

match (a) -[r:CONNECTED]-> (b)
return a, r.transfers as via, b, r.price
order by r.price asc;

MATCH ()-[r:CONNECTED]->()
DELETE r;



